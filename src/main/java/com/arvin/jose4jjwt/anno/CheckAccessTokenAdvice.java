package com.arvin.jose4jjwt.anno;

import com.arvin.jose4jjwt.dto.ApiResult;
import com.arvin.jose4jjwt.service.TokenService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class CheckAccessTokenAdvice {
    @Autowired HttpServletRequest request;
    @Autowired TokenService tokenService;

    @Pointcut("@annotation(com.arvin.jose4jjwt.anno.CheckAccessTokenAnno)")
    private void getAdvicePointcut() {}

    @Around("getAdvicePointcut() && @annotation(checkAccessTokenAnno)")
    public ApiResult auth(ProceedingJoinPoint pj, CheckAccessTokenAnno checkAccessTokenAnno) {
        try {
            System.out.println(checkAccessTokenAnno.title() + " -> " + request.getRemoteAddr() + " -> " + request.getRequestURI());

            String accessToken = request.getHeader("Authorization");
            if(accessToken == null || accessToken.equals("")){
                return ApiResult.Error(5106,"请输入token");
            }

            ApiResult<String> apiResultRet = tokenService.validateAccessToken(accessToken);
            if(!apiResultRet.resultIsSuccess()){
                return ApiResult.Error(apiResultRet.getCode(),apiResultRet.getMsg());
            }
            System.out.println("uid "+apiResultRet.getData() + " 授权访问成功");

            ApiResult apiResult = (ApiResult)pj.proceed();

            return apiResult;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return ApiResult.ErrorService();
        }
    }
}
