package com.arvin.jose4jjwt.service;

import com.arvin.jose4jjwt.dto.ApiResult;
import com.arvin.jose4jjwt.dto.TokenDto;
import com.arvin.jose4jjwt.dto.TokenRetDto;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.FormatterClosedException;
import java.util.List;
import java.util.UUID;

@Service
public class TokenService {
    String tokenPrefix = "Bearer ";
    RsaJsonWebKey rsaJsonWebKey = null;

    RsaJsonWebKey getRsaJsonWebKey() throws JoseException {
        if(rsaJsonWebKey == null){
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            rsaJsonWebKey.setKeyId(UUID.randomUUID().toString());
        }
        return rsaJsonWebKey;
    }

    private TokenDto createToken(String type,String uid,Integer expirationMinutesTime){
        try{
            JwtClaims claims = new JwtClaims();
            claims.setIssuer("enduser");  // 谁创建token并签署它
            claims.setAudience("Audience"); // 打算将令牌发送给谁
            claims.setExpirationTimeMinutesInTheFuture(expirationMinutesTime); // 令牌到期的时间(从现在算起 expirationMinutesTime 分钟)
            claims.setGeneratedJwtId(); // 令牌的唯一标识符
            claims.setIssuedAtToNow();  // 何时发出/创建令牌(现在)
            claims.setNotBeforeMinutesInThePast(2); // 令牌失效之前的时间(2分钟前)
            claims.setSubject("subject"); // 主体/主体是token所涉及的对象
            claims.setClaim("uid",uid); // 可以添加关于主题的其他声明/属性
            claims.setClaim("type",type);

            List<String> groups = Arrays.asList("group-one", "other-group", "group-three");
            claims.setStringListClaim("groups", groups); // 多值声明也可以工作，最终将以JSON数组的形式结束

            JsonWebSignature jws = new JsonWebSignature();
            jws.setPayload(claims.toJson());
            jws.setKey(getRsaJsonWebKey().getPrivateKey());
//            jws.setKeyIdHeaderValue(getRsaJsonWebKey().getKeyId());
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

            String jwt = tokenPrefix + jws.getCompactSerialization();
            System.out.println("JWT: " + jwt);

            Long tick = System.currentTimeMillis() / 1000 + expirationMinutesTime * 60;
            return new TokenDto(jwt,tick);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private JwtClaims parseToken(String token) throws Exception {
        String[] arr = token.split(" ");
        if(arr == null || arr.length != 2){
            throw new FormatterClosedException();
        }

        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime() // JWT必须有过期时间
                .setAllowedClockSkewInSeconds(30) // 允许在验证基于时间的声明时留有一些余地，以说明时钟偏差
                .setRequireSubject() // JWT必须有主题
                .setExpectedIssuer("enduser") // JWT需要由谁出具
                .setExpectedAudience("Audience") // JWT的目标客户
                .setVerificationKey(getRsaJsonWebKey().getKey()) // 使用公钥验证签名
                .setJwsAlgorithmConstraints( // 在给定的上下文中只允许期望的签名算法
                        AlgorithmConstraints.ConstraintType.PERMIT, AlgorithmIdentifiers.RSA_USING_SHA256) // 这里只有RS256
                .build(); // 创建JwtConsumer实例

        JwtClaims jwtClaims = jwtConsumer.processToClaims(arr[1]);
        return jwtClaims;
    }

    public ApiResult<TokenRetDto> createToken(String uid){
        try{
            TokenRetDto token = new TokenRetDto();
            token.setAccessToken(createToken("AccessToken",uid,120));
            token.setRefreshToken(createToken("RefreshToken",uid,1440));

            return ApiResult.Success(token);
        }
        catch (Exception e){
            e.printStackTrace();
            return ApiResult.ErrorService();
        }
    }

    public ApiResult<TokenRetDto> refreshToken(String refreshToken){
        try{
            JwtClaims jwtClaims = parseToken(refreshToken);

            if(jwtClaims != null){
                String type = jwtClaims.getClaimValueAsString("type");
                if(type != null && type.equals("RefreshToken")){
                    System.out.println("refreshToken JWT validation succeeded! " + jwtClaims);

                    String uid = jwtClaims.getClaimValueAsString("uid");
                    System.out.println("uid -> " + uid);

                    return createToken(uid);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return ApiResult.Error(5032,"token 验证失败");
    }

    public ApiResult validateAccessToken(String accessToken){
        try{
            JwtClaims jwtClaims = parseToken(accessToken);

            if(jwtClaims != null){
                String type = jwtClaims.getClaimValueAsString("type");
                if(type != null && type.equals("AccessToken")){
                    System.out.println("JWT validation succeeded! " + jwtClaims);

                    String uid = jwtClaims.getClaimValueAsString("uid");
                    System.out.println("uid -> " + uid);
                    return ApiResult.Success(uid);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return ApiResult.Error(5032,"token 验证失败");
    }
}
