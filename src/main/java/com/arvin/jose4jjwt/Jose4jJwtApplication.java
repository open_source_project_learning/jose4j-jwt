package com.arvin.jose4jjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
public class Jose4jJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(Jose4jJwtApplication.class, args);
    }

}
