package com.arvin.jose4jjwt.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "登录返回的 token")
public class TokenRetDto {

    @ApiModelProperty(value = "接口访问 token")
    private TokenDto accessToken;

    @ApiModelProperty(value = "刷新 token")
    private TokenDto refreshToken;

    public TokenRetDto() {
    }

    public TokenRetDto(TokenDto accessToken, TokenDto refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public TokenDto getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(TokenDto accessToken) {
        this.accessToken = accessToken;
    }

    public TokenDto getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(TokenDto refreshToken) {
        this.refreshToken = refreshToken;
    }
}
