package com.arvin.jose4jjwt.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "token")
public class TokenDto {

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "到期时间的时间戳，单位为秒。超过到期时间后，token 失效。")
    private Long expirationTime;

    public TokenDto() {
    }

    public TokenDto(String token, Long expirationTime) {
        this.token = token;
        this.expirationTime = expirationTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Long expirationTime) {
        this.expirationTime = expirationTime;
    }
}
