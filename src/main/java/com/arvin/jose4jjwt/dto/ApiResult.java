package com.arvin.jose4jjwt.dto;

public class ApiResult<T> {
    private Integer code;
    private String msg;
    private T data;

    public ApiResult(){ }

    public ApiResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ApiResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean resultIsSuccess(){
        return this.code == 200;
    }

    public static ApiResult Success(){
        return new ApiResult(200,"");
    }

    public static ApiResult Success(Object data){
        return new ApiResult(200,"", data);
    }

    public static ApiResult Error(Integer code, String msg){
        return new ApiResult(code,msg);
    }

    public static ApiResult ErrorService(){
        return new ApiResult(5000,"服务异常");
    }
}
