package com.arvin.jose4jjwt.controller;

import com.arvin.jose4jjwt.anno.CheckAccessTokenAnno;
import com.arvin.jose4jjwt.dto.ApiResult;
import com.arvin.jose4jjwt.dto.TokenRetDto;
import com.arvin.jose4jjwt.dto.UserDto;
import com.arvin.jose4jjwt.service.TokenService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"User API"})
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired TokenService tokenService;

    @ApiOperation(value = "用户登录",notes = "用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录名", required = true),
            @ApiImplicitParam(name = "pwd", value = "密码", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "用户登录成功，返回 jwt token")
    })
    @PostMapping("/login")
    public ApiResult<TokenRetDto> login(String loginName, String pwd){
        return tokenService.createToken(loginName);
    }

    @ApiOperation(value = "刷新 token",notes = "刷新 token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "refreshToken", value = "刷新 token", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "刷新 token 成功，返回 jwt token")
    })
    @PostMapping("/refreshToken")
    public ApiResult<TokenRetDto> refreshToken(String refreshToken){
        return tokenService.refreshToken(refreshToken);
    }

    @ApiOperation(value = "用户信息",notes = "用户信息")
    @ApiResponses({
            @ApiResponse(code = 200,message = "用户登录成功，返回 jwt token")
    })
    @GetMapping("/info")
    @CheckAccessTokenAnno(title = "用户信息")
    public ApiResult<UserDto> info(){
        return ApiResult.Success(new UserDto("arvin"));
    }
}
